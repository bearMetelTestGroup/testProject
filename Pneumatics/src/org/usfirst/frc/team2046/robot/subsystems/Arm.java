package org.usfirst.frc.team2046.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc.team2046.robot.*;
import org.usfirst.frc.team2046.robot.commands.*;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Arm extends Subsystem {

	private Arm() {

	}

	public void ExtendPiston1() {

	}

	public void ExtendPiston2() {

	}

	public void RetractPiston1() {

	}

	public void RetractPiston2() {

	}
	
	public void DisarmPiston1() {

	}
	
	public void DisarmPiston2() {

	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub

	}

}

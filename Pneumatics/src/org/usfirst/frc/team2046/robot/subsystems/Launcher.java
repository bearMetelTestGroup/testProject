package org.usfirst.frc.team2046.robot.subsystems;

import org.usfirst.frc.team2046.robot.*;
import org.usfirst.frc.team2046.robot.commands.*;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Launcher extends Subsystem {
	
	private final DoubleSolenoid launchSolenoid1;
	private final DoubleSolenoid launchSolenoid2;
	private final DoubleSolenoid launchSolenoid3;
	private final DoubleSolenoid launchSolenoid4;
	private final Compressor compressor;
	private final static Launcher instance = new Launcher();
	private final AnalogInput pressureSensor;
	private final AnalogInput pressureSensor2;
	
	public static Launcher getInstance() {
		return instance;
	}
	
	private Launcher() {
		launchSolenoid1 = new DoubleSolenoid(RobotMap.PCM_MODULE, RobotMap.PORT_LAUNCHER_EXTEND_1, RobotMap.PORT_LAUNCHER_RETRACT_1);
		launchSolenoid2 = new DoubleSolenoid(RobotMap.PCM_MODULE, RobotMap.PORT_LAUNCHER_EXTEND_2, RobotMap.PORT_LAUNCHER_RETRACT_2);
		launchSolenoid3 = new DoubleSolenoid(RobotMap.PCM_MODULE, RobotMap.PORT_LAUNCHER_EXTEND_3, RobotMap.PORT_LAUNCHER_RETRACT_3);
		launchSolenoid4 = new DoubleSolenoid(RobotMap.PCM_MODULE, RobotMap.PORT_LAUNCHER_EXTEND_4, RobotMap.PORT_LAUNCHER_RETRACT_4);
		pressureSensor = new AnalogInput(RobotMap.PCM_MODULE);
		pressureSensor2 = new AnalogInput(RobotMap.PCM_MODULE);
		compressor = new Compressor(RobotMap.PCM_MODULE);
	}
	
	public void startCompressor() {
		compressor.start();
	}
	
	public void stopCompressor() {
		compressor.stop();
	}
	
	public void ExtendLauncher1() {
		launchSolenoid1.set(DoubleSolenoid.Value.kForward);
	}
	
	public void ExtendLauncher2() {
		launchSolenoid2.set(DoubleSolenoid.Value.kForward);
	}
	
	public void ExtendLauncher3() {
		launchSolenoid3.set(DoubleSolenoid.Value.kForward);
	}
	
	public void ExtendLauncher4() {
		launchSolenoid4.set(DoubleSolenoid.Value.kForward);
	}
	
	public void ExtendBoth(){
		launchSolenoid1.set(DoubleSolenoid.Value.kForward);
		launchSolenoid2.set(DoubleSolenoid.Value.kForward);
	}
	
	public void ExtendBoth2(){
		ExtendLauncher3();
		ExtendLauncher4();
	}
	
	public void RetractLauncher1() {
		launchSolenoid1.set(DoubleSolenoid.Value.kReverse);
	}
	
	public void RetractLauncher2() {
		launchSolenoid2.set(DoubleSolenoid.Value.kReverse);
	}
	
	public void RetractLauncher3() {
		launchSolenoid3.set(DoubleSolenoid.Value.kReverse);
	}
	
	public void RetractLauncher4() {
		launchSolenoid4.set(DoubleSolenoid.Value.kReverse);
	}
	
	public void DisarmLauncher1() {
		launchSolenoid1.set(DoubleSolenoid.Value.kOff);
	}
	
	public void DisarmLauncher2() {
		launchSolenoid2.set(DoubleSolenoid.Value.kOff);
	}
	
	public void DisarmLauncher3() {
		launchSolenoid3.set(DoubleSolenoid.Value.kOff);
	}
	
	public void DisarmLauncher4() {
		launchSolenoid4.set(DoubleSolenoid.Value.kOff);
	}
	
	public double getPressure() {
		return pressureSensor.getVoltage() * 50.0 - 25.0;
	}
	
	public double getPressure2() {
		return pressureSensor2.getVoltage() * 50.0 - 25.0;
	}
	
	
	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		setDefaultCommand(new ControllerCommand());
	}

}

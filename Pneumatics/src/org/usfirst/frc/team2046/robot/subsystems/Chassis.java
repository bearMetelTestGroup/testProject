package org.usfirst.frc.team2046.robot.subsystems;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.interfaces.Gyro;

public class Chassis extends Subsystem{
	private final static Chassis instance = new Chassis();
	private final ADXRS450_Gyro gyro;
	private final Encoder LeftSideEncoder;
	private final int LEFT_SIDE_ENCODER_PORT1 = 1;
	private final int LEFT_SIDE_ENCODER_PORT2 = 2;
	private final Encoder RightSideEncoder;
	private final int RIGHT_SIDE_ENCODER_PORT1 = 3;
	private final int RIGHT_SIDE_ENCODER_PORT2 = 4;

	public Chassis() {
		LeftSideEncoder = new Encoder(LEFT_SIDE_ENCODER_PORT1, LEFT_SIDE_ENCODER_PORT2);
		RightSideEncoder = new Encoder(RIGHT_SIDE_ENCODER_PORT1, RIGHT_SIDE_ENCODER_PORT2);
		gyro = new ADXRS450_Gyro();
		gyro.calibrate();
	}
	
	public double getleftencoderDistance() {
		return LeftSideEncoder.getDistance();
	}
	
	public double getrightencoderDistance() {
		return RightSideEncoder.getDistance();
	}
	
	public double getleftencoderRate() {
		return LeftSideEncoder.getRate();
	}
	
	public double getrightencoderRate() {
		return RightSideEncoder.getRate();
	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		gyro.calibrate();
	}

	public static Chassis getInstance() {
		return instance;
	}

	public double getAverageEncoderValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setForwardSpeed(double d) {
		// TODO Auto-generated method stub
		
	}
}

package org.usfirst.frc.team2046.robot.commands;

import org.usfirst.frc.team2046.robot.subsystems.Chassis;

import edu.wpi.first.wpilibj.command.Command;

public class MoveStraight extends Command{
	private Chassis chassis = Chassis.getInstance();
	private static double distance;
	private static double direction;
	private static double speed;
	private static double startPosition;
	private static double endPosition;

	public MoveStraight(double distance, double speed) {
		this.distance = distance;
		this.speed = speed;
		direction = Math.signum(distance);
		requires(chassis);
	}

	public void initialize() {
		startPosition = chassis.getAverageEncoderValue();
		endPosition = startPosition + distance;
		chassis.setForwardSpeed(speed * direction);
	}

	public void execute() {

	}



	@Override
	protected boolean isFinished() {
		double pos = chassis.getAverageEncoderValue();
		if(direction > 0) {
			return(pos > endPosition);
		}
		else {
			return(pos < endPosition);
		}
	}

	protected void end() {
		chassis.setForwardSpeed(0);
	}

}

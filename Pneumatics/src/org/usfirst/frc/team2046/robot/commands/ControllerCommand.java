package org.usfirst.frc.team2046.robot.commands;

import org.usfirst.frc.team2046.robot.Robot;
import org.usfirst.frc.team2046.robot.operator.XboxController;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class ControllerCommand extends Command {
	
	public ControllerCommand() {
		requires(Robot.launcher);
	}

	@Override
	protected void end() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void execute() {
		// TODO Auto-generated method stub
		if (Robot.oi.getDriverController().getAxisValue(XboxController.RIGHT_TRIGGER_AXIS) > 0.5) {
			Robot.launcher.ExtendLauncher1();
			SmartDashboard.putString("Launcher State", "Extending");
		}
		else if (Robot.oi.getDriverController().getAxisValue(XboxController.LEFT_TRIGGER_AXIS) > 0.5) {
			Robot.launcher.RetractLauncher1();
			SmartDashboard.putString("Launcher State", "Retracting");
		}
		else {
			Robot.launcher.DisarmLauncher1();
			SmartDashboard.putString("Launcher State", "Disarmed");
		}
		
		if (Robot.oi.getDriverController().getButton(XboxController.BUTTON_RIGHT_BUMPER)) {
			Robot.launcher.ExtendLauncher2();
			SmartDashboard.putString("Launcher State", "Extending");
		}
		else if (Robot.oi.getDriverController().getButton(XboxController.BUTTON_LEFT_BUMPER)) {
			Robot.launcher.RetractLauncher2();
			SmartDashboard.putString("Launcher State", "Retracting");
		}
		else {
			Robot.launcher.DisarmLauncher2();
			SmartDashboard.putString("Launcher State", "Disarmed");
		}
		
		SmartDashboard.putNumber("Input Pressure", Robot.launcher.getPressure());
		Robot.oi.getDriverController().setLeftRumble(Robot.launcher.getPressure() > 55.0 ? 1.0f : 0.0f);
		Robot.oi.getDriverController().setRightRumble(Robot.launcher.getPressure() > 55.0 ? 1.0f : 0.0f);
	}

	@Override
	protected void initialize() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void interrupted() {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

}

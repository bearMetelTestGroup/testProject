package autonomous;

import org.usfirst.frc.team2046.robot.subsystems.Launcher;

public class ExtendLauncher extends LauncherCommand {

	private static Launcher launcher = Launcher.getInstance();
	
	public static void extend() {
		launcher.ExtendLauncher1();
		launcher.ExtendLauncher2();
	}
	
	@Override
	protected boolean isFinished() {
		return true;
	}
	
}

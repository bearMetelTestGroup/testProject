package autonomous;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class LauncherCommand extends CommandGroup{
	public LauncherCommand() {
		addSequential(new ExtendLauncher());
		addSequential(new RetractLauncher());
	}

}

package autonomous;

import org.usfirst.frc.team2046.robot.subsystems.Launcher;

public class RetractLauncher extends LauncherCommand {
	private static Launcher launcher = Launcher.getInstance();
	
	public static void retract() {
		launcher.RetractLauncher1();
		launcher.RetractLauncher2();
	}
	
	@Override
	protected boolean isFinished() {
		return true;
	}

}
